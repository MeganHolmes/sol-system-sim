#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#include <time.h>

int*** make3DArray(int,int,int);
void fillWithBlack(int***, int, int, int);
FILE* prepareFile(char*, char*, int, int, int);
void writeToFile(int***, FILE*, int, int, int, char**);
void circl(int***,double,double,double, int[]);
double** getCSVData(char*, int, int);
void add_planets(int, double **, int***);
int position_calculation(double**, int, int, int, double, double, double, double, double, long);
bool check_out_of_frame(double*, int, int);
void remove_planet(int, double**, int);
int make_image(char*, char*, int, int, int, int***, int, double, double, double, int[], int, double**, char**);
void velocity_calculation(double*, double, double, double, double,double);
bool check_crash_in_sun(double*,double,double,double);
char** make_lookup();

int main() {
    //initializations
    char* imgName = "Image";
    char* img_extension = ".ppm";
    char* csvName = "C:\\Users\\megan\\Desktop\\Git\\sol-system-sim\\Planet_List.csv";
    char** lookup = make_lookup();
    int image_height = 1000;
    int image_width = 1000;
    int image_depth = 3; //3 RGB values
    int starRGB [3]= {255,255,0};
    int planetDataPoints = 9;
    int numOfPlanets = 4;
    int image_number = 0;
    int output_image_frequency = 1; //how many ticks in between image output, 1 is one image every tick, 500 is one image every 500 ticks
    long max_simulation_run = 500;
    double starX = image_width/2;
    double starY = image_height/2;
    double starR = 25;
    double star_mass = 1.989 * pow(10,30);
    double gravitational_constant = 6.674 * pow(10,-35.5);
    clock_t start = clock(); //used for timing the program

    printf("Creating image array...\n");
    int*** img =  make3DArray(image_height, image_width, image_depth);


    printf("Importing CSV data...\n");
    double** data = getCSVData(csvName, numOfPlanets, planetDataPoints);


    printf("Entering loop...\n");
    for(long current_simulation_time = 0; current_simulation_time <= max_simulation_run; current_simulation_time++)
    {

        if (current_simulation_time % output_image_frequency == 0)
        {
            image_number = make_image(imgName, img_extension, image_number, image_width, image_height, img, image_depth,
                   starX, starY, starR, starRGB, numOfPlanets, data, lookup);
        }
        else if(numOfPlanets <= 0)
        {
            printf("Tick #%ld%s",current_simulation_time,": ");
            printf("All planets out of frame. ");
            make_image(imgName, img_extension, image_number, image_width, image_height, img, image_depth, starX, starY,
                       starR, starRGB, numOfPlanets, data, lookup);
            printf("\n");
            break;
        }

        numOfPlanets = position_calculation(data,numOfPlanets, image_height, image_width, starX, starY, starR,
                star_mass, gravitational_constant,current_simulation_time);

    }

    clock_t end = clock();
    double time = (double)(end-start)/ CLOCKS_PER_SEC;
    printf("Process took %f seconds.\n", time);

    return 0;
}

char** make_lookup()
{
    char** lookup = calloc(sizeof(char*),256);
    for (int i = 0; i < 256; i++)
    {
        char* sub_char = calloc(sizeof(char),4);
        sprintf(sub_char,"%d",i);
        lookup[i] = sub_char;
    }

    return lookup;
}

void velocity_calculation(double* planet_data, double starX, double starY, double starR, double star_mass,
        double gravitational_constant)
{
    double x_distance = starX - planet_data[0];
    double y_distance = starY - planet_data[1];
    double distance_square = fabs(x_distance*x_distance + y_distance*y_distance);
    double force = -(gravitational_constant * star_mass * planet_data[6] / distance_square); //6 is mass.
    planet_data[7] = planet_data[7] + (force * x_distance / sqrt(distance_square));
    planet_data[8] = planet_data[8] + (force * y_distance / sqrt(distance_square));
}

int make_image(char* imgName, char* img_extension, int image_number, int image_width, int image_height, int*** img,
        int image_depth, double starX, double starY, double starR, int starRGB[], int numOfPlanets, double** data, char** lookup)
{

    fillWithBlack(img, image_height, image_width, image_depth);
    circl(img, starX, starY, starR, starRGB);


    add_planets(numOfPlanets, data, img);


    FILE* imgFile = prepareFile(imgName, img_extension, image_number, image_width, image_height);


    writeToFile(img, imgFile, image_height, image_width, image_depth, lookup);

    image_number++;
    return image_number;
}

int position_calculation(double** data, int numOfPlanets, int height, int width, double starX, double starY,
        double starR, double star_mass, double gravitational_constant, long current_simulation_time)
{
    for (int i = 0; i < numOfPlanets; i++)
    {

        double* planetdata = data[i];
        double planet_velocity_x = planetdata[7];
        double planet_velocity_y = planetdata[8];
        double planet_x = planetdata[0];
        double planet_y = planetdata[1];

        planet_x = planet_x + planet_velocity_x;
        planet_y = planet_y + planet_velocity_y;

        planetdata[0] = planet_x;
        planetdata[1] = planet_y;

        if (check_out_of_frame(planetdata, height, width))
        {
            printf("Tick #%ld%s",current_simulation_time,": ");
            printf("Planet moved out of frame. ");
            remove_planet(numOfPlanets, data, i);
            numOfPlanets--;
            i--;
            printf("\n");
        }
        else if (check_crash_in_sun(planetdata, starX, starY, starR))
        {
            printf("Tick #%ld%s",current_simulation_time,": ");
            printf("Planet crashed into sun. ");
            remove_planet(numOfPlanets, data, i);
            numOfPlanets--;
            i--;
            printf("\n");
        }

        velocity_calculation(planetdata, starX, starY, starR, star_mass, gravitational_constant);
    }
    return numOfPlanets;
}

bool check_crash_in_sun(double* planetdata,double starX, double starY,double starR)
{
    double x_distance = planetdata[0] - starX;
    double y_distance = planetdata[1] - starY;
    double distance = sqrt(x_distance*x_distance  + y_distance*y_distance);
    if (distance <= starR)
    {
        return true;
    }
    else{
        return false;
    }
}

bool check_out_of_frame(double* planetdata, int height, int width)
{
    if ((planetdata[0] - planetdata[2]) < 0)
    {
        return true;
    }
    else if ((planetdata[0] + planetdata[2]) > width)
    {
        return true;
    }
    else if ((planetdata[1] - planetdata[2]) < 0)
    {
        return true;
    }
    else if ((planetdata[1] + planetdata[2]) > height)
    {
        return true;
    }
    else
    {
        return false;
    }
}

void remove_planet(int numOfPlanets, double** data, int planet_number)
{
    for (int i = planet_number + 1; i < numOfPlanets; i++)
    {
        data[i-1] = data[i];
    }
}

void add_planets(int numOfPlanets, double ** data, int*** img)
{
    for (int i = 0; i < numOfPlanets; i++)
    {
        double* planetData = data[i];

        int planetRGB[3] = {planetData[3], planetData[4], planetData[5]};
        circl(img, planetData[0], planetData[1], planetData[2], planetRGB);
    }
}

int*** make3DArray(int height, int width, int depth)
{
    int*** array = calloc(sizeof(int**),height);
    for (int i=0; i<height; i++)
    {
        array[i] = calloc(sizeof(int*),width);
    }
    for (int i=0; i<height; i++)
    {
        for (int j = 0; j<width; j++)
        {
            array[i][j] = calloc(sizeof(int),depth);
        }
    }
    return array;
}

void fillWithBlack(int*** img, int height, int width, int rgbVal)
{
    for (int i=0; i<height; i++)
    {
        for (int j=0; j<width; j++)
        {
            for (int k = 0; k<rgbVal; k++)
            {
                img[i][j][k] = 0;
            }
        }
    }
}

FILE* prepareFile(char* name, char* extension, int number, int width, int height)
{
    char* file_name = calloc(sizeof(char), 13);
    char* number_char = calloc(sizeof(char),5);
    sprintf(number_char, "%i", number);
    file_name = strcat(file_name, name);
    file_name = strcat(file_name, number_char);
    file_name = strcat(file_name, extension);

    FILE* pnmimg;
    if ((pnmimg = fopen(file_name, "wb")) == NULL)
    {
        printf("FILE PREPERATION ERROR!\n");
        exit(1);
    }

    char* header_text = calloc(sizeof(char),18);
    int location = 0;
    int added;

    added = sprintf(header_text,"P3\n");
    location = location + added;
    added = sprintf(header_text+location,"%d %d\n",width,height);
    location = location + added;
    sprintf(header_text+location,"255\n");

    fprintf(pnmimg, "%s", header_text);

    free(file_name);
    free(number_char);
    free(header_text);

    return pnmimg;

}

void writeToFile(int*** img, FILE* file, int height, int width, int rgbVal, char** lookup)
{
    char* text = calloc(sizeof(char), height*width*rgbVal*4); //4 is for 3 numbers for each rgb values with a space. since image is
            //mostly black this is more than we will need.
    long location = 0;
    int added;
    for (int i=0; i<height; i++)
    {
        for (int j=0; j<width; j++)
        {
            for (int k=0; k <rgbVal; k++)
            {
                added = sprintf(text+location,"%s ",lookup[img[i][j][k]]);
                location = location + added;
            }
        }
        added = sprintf(text+location,"\n");
        location = location + added;
    }
    fprintf(file,text);
    free(text);
    fclose(file);
}

void circl(int*** img, double x, double y, double r, int rgbVal[])
{
    double rSquare = r*r; //needed for calculations
    int x_int = x;
    int y_int = y;
    for (int i = -r; i<= r; i++) //starting at x-r position
    {
        for(int j = -r; j <= r; j++) //starting at y-r position
        {
            if((((i)*(i))+((j)*(j))) < rSquare) //if (x^2 +y^2 < r^2)
            {
                for (int k = 0; k < 3; k++)
                {
                    img[x_int + i][y_int + j][k] = rgbVal[k];
                }
            }
        }
    }
}

double** getCSVData(char* name, int rows, int cols)
{
    //opening file
    FILE* csvFile;
    if ((csvFile = fopen(name, "r")) == NULL)
    {
        printf("CSV OPEN ERROR!\n");
        exit(1);
    }

    //after we confirmed the csv file opened we initialize
    char buf[1024];
    char* field;
    int row_count = -2; //offset is so when we actually get to values to store we are at position 0 and can use this
    //variable in the array.
    int field_double;


    //initializing data
    double **data = NULL;
    data = calloc(sizeof(double),rows);
    for (int i = 0; i < rows; i++)
    {
        data[i] = calloc(sizeof(double),cols);
    }


    while (fgets(buf, 1024, csvFile))
    {
        int field_count = 0;
        row_count++;
        if (row_count == -1) continue; //skips file header.

        field = strtok(buf, ",");
        while (field)
        {
            field_double = atof(field);
            double* subarray = data[row_count];
            subarray[field_count] = field_double;

            field = strtok(NULL,",");
            field_count++;
        }
    }
    fclose(csvFile);
    return data;
}
